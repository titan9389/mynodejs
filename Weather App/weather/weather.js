const request = require('request');

var getWeather = (lat, lng, callback) => {
  request({
    url:`https://api.darksky.net/forecast/b6f46e3ecf097e45bb5560e985c4e881/${lat},${lng}`,
    json: true
  }, (error, response, body) => {
    // if(!error && response.statusCode === 200){
    //   callback(undefined,{
    //     temperature:body.currently.temperature,
    //     apparentTemperature:body.currently.apparentTemperature
    //   });
    // }else {
    //   callback('Unable to Fetch Weather');
    // }
    if (error) {
      callback('Unable to connect to forecast.io server.')
    }else if (response.statusCode === 400){
      callback('Unable to Fetch Weather.')
    }else if (response.statusCode === 200){
      callback(undefined, {
      temperature: body.currently.temperature,
      apparentTemperature: body.currently.apparentTemperature
    });
    }
  });
};

module.exports = {
getWeather,
};
